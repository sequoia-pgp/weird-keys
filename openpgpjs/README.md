# OpenPGP.js encryption

This simple project takes an armored key and encrypts sample text to it.

To use first install dependencies:

    npm install

And run it like that:

    node encrypt-using.js ..\null-uid.pgp

Stdout/err will contain armored message or errors.

Bulk testing is possible with:

    for file in *.pgp; do node openpgpjs/encrypt-using.js $file &> openpgpjs/results/$file.txt; done
