const openpgp = require('openpgp');
const fs = require('fs');

const result = openpgp.key.readArmored(fs.readFileSync(process.argv[2], 'utf-8'));

if (result.err) {
    console.error(result.err);
    process.exit(1);
}

const options = {
    data: 'Hello, World!',
    publicKeys: result.keys
};

openpgp.encrypt(options).then(result => console.log(result.data), console.error.bind(console));
